@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-">
            @foreach($posts as $p)
            <div class="card">
                <div class="card-header">
                    <img src="{{ Auth::user()->avatar }}" alt="ss" width="40px" height="40px" style="border-radius:80%">
                    {{ Auth::user()->name }}
                    </div>
                    <div class="card-body">                        
                        <img src="{{ $p->image }}" alt="ss" width="500px" height="500px">
                    </div>
                    <div class="card-footer">
                        <b>{{ Auth::user()->name }}</b>
                        <p>{{ $p->caption}}</p>
                    </div>                                   
                
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
