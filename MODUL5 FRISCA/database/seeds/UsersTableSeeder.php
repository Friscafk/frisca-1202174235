<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    public function run()

    {
        DB::table('users')->insert([
            'name' => 'Frisca',
            'email' => 'friscafebriyani23@gmail.com',
            'password' => bcrypt('password'),
            'title' => 'Hello People!',
            'description' => 'Always be Happy',
            'url'=> 'h', 
            'avatar'=> 'image/ica.jpg'
            ]);
            }   
    }
    
?>