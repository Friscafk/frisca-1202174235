<?php 
	session_start();
	if (!(isset($_SESSION["id"]))) {
		header("Location: home.php");
	}
 ?>
<!DOCTYPE html>
<html>

<head>
	<title>Cart</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color:pink; border-color:white;">
		<a class="navbar-brand" href="home.php"><img src="EAD.png" width="200"></a>
		<ul class="nav justify-content-end">
			<a class="nav-link text-white" href="pembelian.php"><img src="shopping-cart.jpg" width="20"></a>
			<li class="nav-item dropdown active">
				<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					<?php echo $_SESSION['username']; ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="profile.php">Profile</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="logout.php">Log Out</a>
				</div>
			</li>
		</ul>
	</nav>
	<h3 class="text-center my-3">Cart</h3>
	<div class="table-responsive">
		<table class="table table-bordered mx-auto mt-5" style="width: 58rem;">
			<thead>
				<tr>
					<th scope="col">No</th>
					<th scope="col">Product</th>
					<th scope="col">Price</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<?php 
	  	$koneksi = mysqli_connect('localhost','root','','db_wad_04');
	  	$idUser = $_SESSION['id'];
			$cart = "SELECT * FROM `cart` WHERE `user_id`='".$idUser."'";
			$query = mysqli_query($koneksi, $cart);	
			$price = 0;
			if (mysqli_num_rows($query) > 0) {
				$i = 1;
				while ($hasil = mysqli_fetch_assoc($query)) {
				?>
				<tr>
					<th scope="row">
						<?php echo $i; $i++;?>
					</th>
					<td>
						<?php echo $hasil["product"]; ?>
					</td>
					<td>Rp
						<?php echo $hasil["price"]; $price += $hasil["price"];?>
					</td>
					<td><a href="deletecart.php?idCart=<?php echo $hasil["id"]; ?>" class="btn btn-danger">X</a></td>
				</tr>
				<?php	}
			} else {?>
				<tr>
					<th scope="row" colspan="4" class="text-center">Cart Empty</th>
				</tr>
				<?php
			}
	  	 ?>
				<tr>
					<th scope="row" colspan="2" class="text-center">Total</th>
					<th colspan="2">Rp
						<?php echo $price; ?>
					</th>
				</tr>
			</tbody>
		</table>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="footer text-center pt-4">© EAD STORE</div>
		</div>
	</footer>
</body>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</html>