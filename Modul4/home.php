<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>

<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color:pink; border-color:white;">
		<a class="navbar-brand" href="home.php"><img src="EAD.png" width="200"></a>
		<ul class="nav justify-content-end">
			<?php if (isset($_SESSION["id"])) {?>
			<a class="nav-link text-white" href="daftarcart.php"><img src="shopping-cart.jpg" width="20"></a>
			<li class="nav-item dropdown active">
				<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					<?php echo $_SESSION['username']; ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="profile.php">Profile</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="logout.php">Log Out</a>
				</div>
			</li>
			<?php } else {?>
			<li class="nav-item active">
				<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#login">Login</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#register">Register</a>
			</li>
			<?php } ?>
		</ul>
	</nav>
	<div class="card my-3 mx-auto text-white" style="width: 78%; background-color:pink; border-color:white;">
		<div class="card-body">
			<span class="align-middle">
				<h1>Hello Coders</h1>
				<p>Welcome to our store, please a look for the product you might buy</p>
			</span>
		</div>
	</div>
	<div class="row mx-auto" style="width: 78%;">
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 20rem;">
				<img src="web.jpg" class="card-img-top" alt="...">
				<div class="card-body" style="height: 15rem;">
					<h5 class="card-title">Learning Basic Web Programming</h5>
					<p class="card-text"><b>Rp.125.000,-</b></p>
					<p class="card-text">Situs web adalah kumpulan halaman web yang saling berhubungan yang berisikan kumpulan informasi yang disediakan secara perorangan, kelompok, atau organisasi.</pre>
				</div>
				<div class="card-body">
					<a href="pembelian.php?product=Learning+Basic+Web+Programming&price=125000" class="card-link btn btn-primary" role="button"
					 style="width: 17rem; background-color:pink; border-color:white;">Buy</a>
				</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 20rem;">
				<img src="java.jpg" class="card-img-top" alt="...">
				<div class="card-body" style="height: 15rem;">
					<h5 class="card-title">Starting Programming in Java</h5>
					<p class="card-text"><b>Rp.175.000,-</b></p>
					<p class="card-text">Java adalah bahasa pemrograman yang dapat dijalankan di berbagai komputer termasuk telepon
						genggam.</p>
				</div>
				<div class="card-body">
					<a href="pembelian.php?product=Starting+Programming+In+Java&price=175000" class="card-link btn btn-primary" role="button"
					 style="width: 17rem; background-color:pink; border-color:white;">Buy</a>
				</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 20rem;">
				<img src="phyton.jpg" class="card-img-top" alt="...">
				<div class="card-body" style="height: 15rem;">
					<h5 class="card-title">Starting Programming in Python</h5>
					<p class="card-text"><b>Rp.250.000,-</b></p>
					<p class="card-text">Python adalah bahasa pemrograman interpretatif multiguna dengan filosofi perancangan yang
						berfokus pada tingkat keterbacaan kode.</p>
				</div>
				<div class="card-body">
					<a href="pembelian.php?product=Starting+Programming+In+Python&price=250000" class="card-link btn btn-primary" role="button"
					 style="width: 17rem;background-color:pink; border-color:white;">Buy</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form method="post" action="login.php">
					<div class="modal-header">
						<h5 class="modal-title" id="loginLabel">Login</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-email" class="col-form-label">Email Address</label>
							<input type="email" class="form-control" name="loginEmail" placeholder="Enter Email" required>
						</div>
						<div class="form-group">
							<label for="recipient-password" class="col-form-label">Password</label>
							<input type="password" class="form-control" name="loginPassword" placeholder="Enter Password" required>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-info" data-dismiss="modal" value="Close">
						<input type="submit" class="btn btn-success" value="Login">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="registerLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form method="post" action="register.php">
					<div class="modal-header">
						<h5 class="modal-title" id="registerLabel">Register</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-email" class="col-form-label">Email Address</label>
							<input type="email" class="form-control" name="registEmail" placeholder="Enter Email" required>
						</div>
						<div class="form-group">
							<label for="recipient-username" class="col-form-label">Username</label>
							<input type="text" class="form-control" name="registUsername" placeholder="Enter Username" required>
						</div>
						<div class="form-group">
							<label for="recipient-password" class="col-form-label">Password</label>
							<input type="password" class="form-control" name="registPassword" placeholder="Password" required>
						</div>
						<div class="form-group">
							<label for="message-repassword" class="col-form-label">Confirm Password</label>
							<input type="password" class="form-control" name="registConPassword" placeholder="Confirm Password" required>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-info" data-dismiss="modal" value="Close">
						<input type="submit" class="btn btn-success" value="Register">
					</div>
				</form>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="footer text-center py-3">© EAD STORE</div>
		</div>
	</footer>
</body>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>