<?php 
	session_start();
	if (!(isset($_SESSION["id"]))) {
		header("Location: home.php");
	}
 ?>
<!DOCTYPE html>
<html>

<head>
	<title>Profile</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
	<nav class="navbar navbar-light" style="background-color:pink; border-color:white;">
		<a class="navbar-brand" href="home.php"><img src="EAD.png" width="200"></a>
		<ul class="nav justify-content-end">
			<a class="nav-link text-white" href="pembelian.php"><img src="shopping-cart.jpg" width="20"></a>
			<li class="nav-item dropdown active">
				<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					<?php echo $_SESSION['username']; ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="profile.php">Profile</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="logout.php">Log Out</a>
				</div>
			</li>
		</ul>
	</nav>
	<div class="card w-75 my-3 mx-auto">
		<div class="card-body">
			<form action="update.php" method="post">
				<div class="row text-center">
					<div class="col-12">
						<h4>Profile</h4>
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-3">
						<label>Email</label>
					</div>
					<div class="col-sm-9">
						<label>
							<?php echo $_SESSION['email']; ?>
						</label>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-sm-3">
						<label>Username</label>
					</div>
					<div class="col-sm-9">
						<input type="text" name="username" required placeholder="Username" class="form-control" value="<?php echo $_SESSION['username']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<label>Mobile Number</label>
					</div>
					<div class="col-sm-9">
						<input type="number" name="mobileNumber" required placeholder="Mobile Number" class="form-control" value="<?php echo $_SESSION['mobile_number']; ?>">
					</div>
				</div>
				<hr>
				<div class="row mb-3">
					<div class="col-sm-3">
						<label>New Password</label>
					</div>
					<div class="col-sm-9">
						<input type="password" name="newPassword" required placeholder="New Password" class="form-control">
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-sm-3">
						<label>Confirm Password</label>
					</div>
					<div class="col-sm-9">
						<input type="password" name="confirmPassword" required placeholder="Confirm Password" class="form-control">
					</div>
				</div>
				<div class="row text-center mb-1">
					<div class="col-12">
						<input type="submit" value="Save" class="btn btn-info btn-block" class="form-control">
						<a href="home.php" class="btn btn-outline-info btn-block">Cancel</a>
					</div>
				</div>
			</form>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<div class="footer text-center pt-4">© EAD STORE</div>
		</div>
	</footer>
</body>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</html>