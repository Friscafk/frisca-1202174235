<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Divisi Kepelatihan 2019',
            'image' =>  'image/kepelatihan.jpg',
            'likes' =>  0,
            ]);
          
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Kari Ayam',
            'image' =>  'image/02.jpg',
            'likes' =>  0,
            ]);
        }   
    }

?>