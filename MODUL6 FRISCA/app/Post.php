<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'user_id', 'caption', 'image', 'likes',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }

    public function kometarPosts()
    {
        return $this->hasMany(\App\Komentar_Post::class,'post_id');   
    }
}
