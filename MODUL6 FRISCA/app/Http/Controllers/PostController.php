<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Komentar_Post;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('imagePost');

        if (!$file) {
            return redirect()->route('user.index');
        }

        $file_name = $file->getClientOriginalName();
        $path = public_path("/posts/".Auth::User()->id);
        $file->move($path, $file_name);

        $posts = new Post();
        $posts->user_id = Auth::User()->id;
        $posts->caption = $request->captionPost;
        $posts->image = "/posts/".Auth::User()->id."/".$file_name;
        $posts->likes = 0;

        $posts->save();

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with('user')->where('id', $id)->get();
        $comments = Komentar_Post::with('user')->where('post_id', $id)->get();
        return view('post.detailpost',compact('post','comments')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
