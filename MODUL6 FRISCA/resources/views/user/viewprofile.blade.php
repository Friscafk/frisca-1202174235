@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-5">
        <div class="col-md-4 text-center">
            <img src="{{$profile[0]->avatar}}" width="60%">
        </div>
        <div class="col-md-6">
            <h4>{{$profile[0]->name}}</h4><br>
            <p><a href="{{ route('user.create') }}" >Edit Profile</a><br>
                <b>{{count($profile[0]->posts)}}</b> Post</p>
            <p><b>{{$profile[0]->title}}</b><br>{{$profile[0]->description}} <br><a href="{{$profile[0]->url}}" target="_blank">{{$profile[0]->url}}</a> 
            </p>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{ route('post.create') }}" >Add New Post</a>
        </div>
    </div>
    <div class="row justify-content-left my-1">
        @foreach($profile[0]->posts as $post)
        <div class="col-md-4 my-3">
            <a href="{{ route('post.show', $post->id) }}"><img src="{{$post->image}}" width="100%"></a>
        </div>
        @endforeach
    </div>
</div>
@endsection
