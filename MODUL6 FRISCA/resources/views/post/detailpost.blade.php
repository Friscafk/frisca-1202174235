@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 text-center">
            <img src="{{asset($post[0]->image)}}" width="75%">
        </div>
        <div class="col-md-4">
            <img src="{{asset($post[0]->user->avatar)}}" width="50" style="border-radius: 50%;">
            <b> {{$post[0]->user->name}}</b> 
            <br>
            <hr>
            <p><b>{{$post[0]->user->email}}</b> {{$post[0]->caption}}</p>
            <p>
                @foreach($comments as $komentar)
                <b>{{$komentar->user->email}}</b> {{$komentar->comment}} <br>
                @endforeach
            </p>
            <hr>
            <p>
                <a href="{{ url('comment/'.$post[0]->id) }}">
                    <i class="fa fa-heart-o" style="font-size:24px; color: black;"></i>
                </a> 
                <a href="#detail_komentar">
                    <i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i>
                </a>
                <b>{{$post[0]->likes}} Like</b>
            </p>
            <form action="{{route('comment.add')}}" method="POST">  
                @csrf
                <input type="hidden" name="post_id" value="{{$post[0]->id}}">
                <div class="input-group">
                    <input type="text" class="form-control" name="komentar" id="detail_komentar" required placeholder="Add a comment..">
                    <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Send</button>
                </div>
            </form>    
        </div>
    </div>
</div>
@endsection
