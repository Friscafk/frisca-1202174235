@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-">
            @foreach($posts as $p)
            <div class="card">
                <div class="card-header">
                    <img src="{{ $p->user->avatar }}" alt="ss" width="40px" height="40px" style="border-radius:80%">
                    {{ $p->user->name }}
                </div>
                <div class="card-body">                        
                    <img src="{{ $p->image }}" alt="ss" width="500px" height="500px">
                </div>
                <div class="card-footer">
                    <p>
                        <a href="{{ url('comment/'.$p->id) }}">
                            <i class="fa fa-heart-o" style="font-size:24px; color: black;"></i>
                        </a> 
                        <a href="#detail_komentar">
                            <i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i>
                        </a>
                        <b>{{$p->likes}} Like</b>
                    </p>
                    <b>{{ $p->user->email }}</b>
                    <p>{{ $p->caption}}</p>
                    <p>Comment <br>
                        @foreach($p->kometarPosts as $komentar)
                            @foreach($users as $user)
                                @if($komentar->user_id == $user->id && $p->id == $komentar->post_id)
                                    <b>{{$user->email}}</b> {{$komentar->comment}}<br>
                                @endif
                            @endforeach
                        @endforeach
                    </p>
                    <form action="{{route('comment.add')}}" method="POST">  
                        @csrf
                        <input type="hidden" name="post_id" value="{{$p->id}}">
                        <div class="input-group">
                            <input type="text" class="form-control" name="komentar" id="detail_komentar" required placeholder="Add a comment..">
                            <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Send</button>
                        </div>
                    </form>
                </div>                                     
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
